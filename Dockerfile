FROM python:3.8-alpine
MAINTAINER BryanR

# Install requests

RUN pip3 install requests argparse datetime pytz requests.auth

# Copy application files into container
COPY . /astronomy

ENTRYPOINT ["/astronomy/solar.py"]
